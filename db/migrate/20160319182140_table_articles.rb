class TableArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :titre
      t.string :contenu

      t.timestamps null: false
    end
  end
end