class Comment < ActiveRecord::Base
  belongs_to :articles, touch: true
  
  validates :author, presence: {message: "L'auteur du commentaire ne dois pas etre vide."}
  validates :text, presence: {message: "Le texte du commentaire ne doit pas être vide."}
 
  scope :by_article, ->(article_id) { where(article_id: article_id) }
  scope :by_author, ->(author) { where(author: author) }   
end
