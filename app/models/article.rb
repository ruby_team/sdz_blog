class Article < ActiveRecord::Base
  has_many :comments
  accepts_nested_attributes_for :comments, allow_destroy: true, reject_if: :new_record?
  
  validates_presence_of :titre
  validates_presence_of :contenu
  
  def reject_article(attributed)
    attributed['titre'].blank?
  end   
end