class ArticlesController < ApplicationController
  def index
    @articles = Article.page(params[:page]).per(params[:per_page]).order(id: :desc)
  end
  
  def show
    @article = Article.find(params[:id])
  end
  
  def update
    @article = Article.find(params[:id])
    @comment = Comment.create author: params[:author], text: params[:text], article_id: @article.id
    flash[:success] = "Le commentaire a été ajouter. (id: "+@comment.id.to_s+", auteur:"+params[:author]+")"
    render 'show'
  end
end