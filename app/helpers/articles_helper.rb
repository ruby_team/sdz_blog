module ArticlesHelper
  def article_link the_article
    html = "<a href='/articles/#{the_article.id}'>".html_safe
    html += the_article.titre
    html += "</a>".html_safe
    html
  end
end